//=============================================================================================
// Szamitogepes grafika hazi feladat keret. Ervenyes 2014-tol.          
// A //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// sorokon beluli reszben celszeru garazdalkodni, mert a tobbit ugyis toroljuk. 
// A beadott program csak ebben a fajlban lehet, a fajl 1 byte-os ASCII karaktereket tartalmazhat. 
// Tilos:
// - mast "beincludolni", illetve mas konyvtarat hasznalni
// - faljmuveleteket vegezni (printf is fajlmuvelet!)
// - new operatort hivni az onInitialization fßggvÊnyt kivÊve, a lefoglalt adat korrekt felszabadítåsa nÊlkßl 
// - felesleges programsorokat a beadott programban hagyni
// - tovabbi kommenteket a beadott programba irni a forrasmegjelolest kommentjeit kiveve
// ---------------------------------------------------------------------------------------------
// A feladatot ANSI C++ nyelvu forditoprogrammal ellenorizzuk, a Visual Studio-hoz kepesti elteresekrol
// es a leggyakoribb hibakrol (pl. ideiglenes objektumot nem lehet referencia tipusnak ertekul adni)
// a hazibeado portal ad egy osszefoglalot.
// ---------------------------------------------------------------------------------------------
// A feladatmegoldasokban csak olyan gl/glu/glut fuggvenyek hasznalhatok, amelyek
// 1. Az oran a feladatkiadasig elhangzottak ES (logikai AND muvelet)
// 2. Az alabbi listaban szerepelnek:  
// Rendering pass: glBegin, glVertex[2|3]f, glColor3f, glNormal3f, glTexCoord2f, glEnd, glDrawPixels
// Transzformaciok: glViewport, glMatrixMode, glLoadIdentity, glMultMatrixf, gluOrtho2D, 
// glTranslatef, glRotatef, glScalef, gluLookAt, gluPerspective, glPushMatrix, glPopMatrix,
// Illuminacio: glMaterialfv, glMaterialfv, glMaterialf, glLightfv
// Texturazas: glGenTextures, glBindTexture, glTexParameteri, glTexImage2D, glTexEnvi, 
// Pipeline vezerles: glShadeModel, glEnable/Disable a kovetkezokre:
// GL_LIGHTING, GL_NORMALIZE, GL_DEPTH_TEST, GL_CULL_FACE, GL_TEXTURE_2D, GL_BLEND, GL_LIGHT[0..7]
//
// NYILATKOZAT
// ---------------------------------------------------------------------------------------------
// Nev    : <VEZETEKNEV(EK)> <KERESZTNEV(EK)>
// Neptun : <NEPTUN KOD>
// ---------------------------------------------------------------------------------------------
// ezennel kijelentem, hogy a feladatot magam keszitettem, es ha barmilyen segitseget igenybe vettem vagy 
// mas szellemi termeket felhasznaltam, akkor a forrast es az atvett reszt kommentekben egyertelmuen jeloltem. 
// A forrasmegjeloles kotelme vonatkozik az eloadas foliakat es a targy oktatoi, illetve a 
// grafhazi doktor tanacsait kiveve barmilyen csatornan (szoban, irasban, Interneten, stb.) erkezo minden egyeb 
// informaciora (keplet, program, algoritmus, stb.). Kijelentem, hogy a forrasmegjelolessel atvett reszeket is ertem, 
// azok helyessegere matematikai bizonyitast tudok adni. Tisztaban vagyok azzal, hogy az atvett reszek nem szamitanak
// a sajat kontribucioba, igy a feladat elfogadasarol a tobbi resz mennyisege es minosege alapjan szuletik dontes.  
// Tudomasul veszem, hogy a forrasmegjeloles kotelmenek megsertese eseten a hazifeladatra adhato pontokat 
// negativ elojellel szamoljak el es ezzel parhuzamosan eljaras is indul velem szemben.
//=============================================================================================

#define _USE_MATH_DEFINES
#include <math.h>
#include <stdlib.h>

#if defined(__APPLE__)                                                                                                                                                                                                            
#include <OpenGL/gl.h>                                                                                                                                                                                                            
#include <OpenGL/glu.h>                                                                                                                                                                                                           
#include <GLUT/glut.h>                                                                                                                                                                                                            
#else                                                                                                                                                                                                                             
#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__)                                                                                                                                                                       
#include <windows.h>                                                                                                                                                                                                              
#endif                                                                                                                                                                                                                            
#include <GL/gl.h>                                                                                                                                                                                                                
#include <GL/glu.h>                                                                                                                                                                                                               
#include <GL/glut.h>                                                                                                                                                                                                              
#endif          
#include <iostream>
#include <sstream>
#include <stdio.h>

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
// Innentol modosithatod...

//hazi 2
bool debug = false;
struct float3;
class CameraSuper{
public:
	virtual float3 getScreenPoint(float3 worldPoint) = 0;

};
CameraSuper* debugCameraReach;



const float epsilon = 0.001f;
float negyzet(float x){
	return x*x;
}

float kob(float x){
	return x*x*x;
}

struct float3 {
	float x, y, z;

	float3() {
		x = y = z = 0;
	}
	float3(float x0, float y0, float z0 = 0) {
		x = x0; y = y0; z = z0;
	}
	float3 operator*(float a) const {
		return float3(x * a, y * a, z * a);
	}
	float3 operator/(float a) const {
		return float3(x / a, y / a, z / a);
	}
	float3 operator+(const float3& v) const {
		return float3(x + v.x, y + v.y, z + v.z);
	}
	float3 operator+=(const float3& v) {
		x = x + v.x;
		y = y + v.y;
		z = z + v.z;
		return (*this);
	}
	float3 operator-(const float3& v) const {
		return float3(x - v.x, y - v.y, z - v.z);
	}
	float operator*(const float3& v) const { 	// dot product
		return (x * v.x + y * v.y + z * v.z);
	}
	float3 operator%(const float3& v) const { 	// cross product
		return float3(y*v.z - z*v.y, z*v.x - x*v.z, x*v.y - y*v.x);
	}
	float length() { return sqrt(x * x + y * y + z * z); }

	//A vektor egység hosszú verziójával tér vissza
	float3 normalize(){
		float3 retVal(x, y, z);
		return retVal / length();
	}
	void rotateX(float degree){
		float yOld = y;
		float zOld = z;
		degree *= 0.0174532925;
		y = yOld*cos(degree) + zOld*-1 * sin(degree);
		z = yOld*sin(degree) + zOld*cos(degree);
	}
	void rotateY(float degree){
		degree = (degree*M_PI) / 180;
		float xOld = x;
		float zOld = z;
		x = xOld*cos(degree) + zOld*sin(degree);
		z = xOld*-1 * sin(degree) + zOld*cos(degree);

	}
	void rotateZ(float degree){
		degree *= 0.0174532925;
		float xOld = x;
		float yOld = y;
		x = xOld*cos(degree) + yOld*-1 * sin(degree);
		y = xOld*sin(degree) + yOld*cos(degree);
	}
	std::string toString(){
		std::ostringstream oss;
		oss << "x=" << x << " y=" << y << " z=" << z << " ";
		std::string tmp = oss.str();
		return tmp;
	}

};

float3 multiply(float3 a, float3 b){
	return float3(a.x*b.x, a.y*b.y, a.z*b.z);
}


float3 displayPoints[300];
float3 displayPointColors[300];
int numDisplayPoints = 0;


class Material{
public:
	float3 F0;
	float  n;
	float3 kd, ks, ka;
	float  shininess;
	boolean isRough, isRefracting, isReflecting;
	std::string name;
	float3 reflect(float3 inDir, float3 normal) {
		return (inDir - normal * (normal*inDir) * 2.0f).normalize();
	}
	float3 refract(float3 inDir, float3 normal) {
		float ior = n;
		float cosa = -1 * (normal*inDir);
		if (cosa < 0) { 
			cosa = -cosa; 
			normal = normal * -1; 
			ior = 1 / n;
		}
		float disc = 1 - (1 - negyzet(cosa)) / negyzet(ior);
		if (disc < 0)
			return reflect(inDir, normal);
		return (inDir / ior + normal * (cosa / ior - sqrt(disc))).normalize();
	}
	float3 Fresnel(float3 inDir, float3 normal) {
		float cosa = fabs(normal * inDir);
		float3 F0inv = float3(1, 1, 1) - F0;
		return F0 + F0inv * pow(1 - cosa, 5);
	}
	float3 shade(float3 normal, float3 viewDir,float3 lightDir, float3 inRad)
	{
		float3 reflRad(0, 0, 0);
		float cosTheta = normal * lightDir;
		if (cosTheta < 0) 
			return reflRad;
		reflRad = multiply(inRad, kd) * cosTheta;
		float3 halfway = (viewDir + lightDir).normalize();
		float cosDelta = normal * halfway;
		if (cosDelta < 0) 
			return reflRad;
		return reflRad + multiply(inRad, ks) * pow(cosDelta, shininess);
	}


};

struct Hit {
	float t;
	float3 position;
	float3 normal;
	Material* material;
	Hit() { t = -1; }
};

class Ray{
private:
	Ray(){}
public:
	float3 origin;
	float3 v;
	Ray(float3 origin, float3 v){
		this->origin = origin;
		this->v = v.normalize();
	}
};

class Intersectable{
public:
	Material* material;
	virtual Hit intersect(const Ray& ray) = 0;
};

class Triangle : public Intersectable{
public:
	float3 a;
	float3 b;
	float3 c;

	Triangle(){
	}

	Triangle(float3 a, float3 b, float3 c){
		this->a = a;
		this->b = b;
		this->c = c;
	}

	Hit intersect(const Ray& ray) {
		Hit hit;
		hit.material = NULL;
				
		float3 n = (b - a) % (c - a);
		//n = n.normalize();
		if (n * ray.v > 0){
			n = n * -1;
		}

		//Ellenőrizzük, hogy a háromszög síkjával párhuzamos-e az egyenes.
		//Ha igen azt onnan tudjuk, hogy a háromszög normálvektora merőleges az egyenesre.
		if (ray.v*n == 0){
			hit.t = -1;
			return hit;
		}

		float t = ((a-ray.origin)*n) / (ray.v*n);
		if (t < 0){
			hit.t = -1;
			return hit;
		}

		//az origóból az ütközésre
		float3 x = ray.origin + (ray.v * t);

		bool in = true;
		if (((b - a) % (x - a)) * n < 0){
			in = false;
		}
		if (((c - b) % (x - b)) * n < 0){
			in = false;
		}
		if (((a - c) % (x - c)) * n < 0){
			in = false;
		}
		if (!in){
			hit.t = -1;
			return hit;
		}

		hit.position = x;
		hit.t = t;
		hit.normal = n.normalize();

		if (debug){
			float3 screenPoint1 = debugCameraReach->getScreenPoint(a);
			float3 screenPoint2 = debugCameraReach->getScreenPoint(b);
			float3 screenPoint3 = debugCameraReach->getScreenPoint(c);

			displayPointColors[numDisplayPoints] = float3(0, 0, 1);
			displayPoints[numDisplayPoints++] = screenPoint1;
			displayPointColors[numDisplayPoints] = float3(0, 0, 1);
			displayPoints[numDisplayPoints++] = screenPoint2;

			displayPointColors[numDisplayPoints] = float3(0, 0, 1);
			displayPoints[numDisplayPoints++] = screenPoint3;
			displayPointColors[numDisplayPoints] = float3(0, 0, 1);
			displayPoints[numDisplayPoints++] = screenPoint2;

			displayPointColors[numDisplayPoints] = float3(0, 0, 1);
			displayPoints[numDisplayPoints++] = screenPoint1;
			displayPointColors[numDisplayPoints] = float3(0, 0, 1);
			displayPoints[numDisplayPoints++] = screenPoint3;

		}



		return hit;
	}
/*	Hit intersect(const Ray& ray) {
		Hit hit;
		hit.material = NULL;
				
		//projecting
		float3 p0 = a;
		float3 n = (b-c).normalize();
		float3 l0 = ray.origin;
		float3 l = ray.v;
		l = l.normalize();

		if ((l * n) == 0){
			hit.t = -1;
			return hit;
		}
		float d = ((p0 - l0) * n) / (l * n);
		float3 projectedPoint = l0 + l * d;

		bool in = false;
		float3 r = (((a + b) / 2) + c) / 2;		

		float3 ac = c - a;
		float3 ax = projectedPoint - a;
		float3 ar = r - a;
		if ((ac%ax)*(ac%ar)<0){
			in = false;
		}

		float3 ab = b - a;
		if ((ab%ax)*(ab%ar)<0){
			in = false;
		}

		float3 bc = c - b;
		float3 bx = projectedPoint - b;
		float3 br = r - b;
		if ((bc%bx)*(bc%br)<0){
			in = false;
		}


		if (!in){
			hit.t = -1;
			return hit;
		}

		hit.position = projectedPoint;
		hit.t = d;
		hit.normal = n;

		if (debug){
			float3 screenPoint1 = debugCameraReach->getScreenPoint(a);
			float3 screenPoint2 = debugCameraReach->getScreenPoint(b);
			float3 screenPoint3 = debugCameraReach->getScreenPoint(c);

			displayPointColors[numDisplayPoints] = float3(0, 0, 1);
			displayPoints[numDisplayPoints++] = screenPoint1;
			displayPointColors[numDisplayPoints] = float3(0, 0, 1);
			displayPoints[numDisplayPoints++] = screenPoint2;

			displayPointColors[numDisplayPoints] = float3(0, 0, 1);
			displayPoints[numDisplayPoints++] = screenPoint3;
			displayPointColors[numDisplayPoints] = float3(0, 0, 1);
			displayPoints[numDisplayPoints++] = screenPoint2;

			displayPointColors[numDisplayPoints] = float3(0, 0, 1);
			displayPoints[numDisplayPoints++] = screenPoint1;
			displayPointColors[numDisplayPoints] = float3(0, 0, 1);
			displayPoints[numDisplayPoints++] = screenPoint3;

		}



		return hit;
	}*/
};

const int CHECKER_SIZE = 30;
class TexturedSquare : public Intersectable {
public:
	float3 a;
	float3 b;
	float3 c;
	float3 d;
	Material* mat1;
	Material* mat2;
	bool check(){
		float3 ab = b - a;
		float3 ac = c - a;
		float3 ad = d - a;
		return (((ab % ac) * ad) == 0);
	}
	Hit intersect(const Ray& ray) {
		bool originalDebug = debug;
		debug = false;

		Triangle tri1;
		tri1.a = a;
		tri1.b = b;
		tri1.c = c;
		Hit hit1 = tri1.intersect(ray);

		Triangle tri2;
		tri2.a = a;
		tri2.b = c;
		tri2.c = d;
		Hit hit2 = tri2.intersect(ray);

		Hit hit;
		if (hit1.t > 0)
			hit = hit1;
		if (hit2.t > 0)
			hit = hit2;
		if (hit1.t < 0 && hit2.t < 0){
			hit.t = -1;
			debug = originalDebug;
			return hit;
		}


		float3 x = hit.position;
		float side1Proj = (x - a)*(b - a).normalize();
		float side2Proj = (x - a)*(d - a).normalize();
		float side1 = side1Proj / (b - a).length();
		float side2 = side2Proj / (d - a).length();
		int tmp1 = (int)(side1 * CHECKER_SIZE);
		int tmp2 = (int)(side2 * CHECKER_SIZE);
		int tmp = (tmp1 + tmp2) % 2;
		if (tmp == 0){
			hit.material = mat1;
		} else {
			hit.material = mat2;
		}
		debug = originalDebug;
		return hit;
	}
};

class OpenBox : public Intersectable {
	TexturedSquare squares[6];
	bool initalized;
public:
	float3 p[8];
	Material* materials[12];
	OpenBox(){
		initalized = false;
	}
	void initalize(){
		for (int i = 0; i <= 5; ++i){
			squares[i].mat1 = materials[i * 2];
			squares[i].mat2 = materials[i * 2 + 1];
		}

		squares[0].a = p[2];
		squares[0].b = p[1];
		squares[0].c = p[5];
		squares[0].d = p[6];

		squares[1].a = p[2];
		squares[1].b = p[6];
		squares[1].c = p[7];
		squares[1].d = p[3];

		squares[2].a = p[3];
		squares[2].b = p[7];
		squares[2].c = p[4];
		squares[2].d = p[0];

		squares[3].a = p[4];
		squares[3].b = p[5];
		squares[3].c = p[1];
		squares[3].d = p[0];

		squares[4].a = p[7];
		squares[4].b = p[6];
		squares[4].c = p[5];
		squares[4].d = p[4];

		squares[5].a = p[1];
		squares[5].b = p[2];
		squares[5].c = p[3];
		squares[5].d = p[0];

		initalized = true;
	}
	bool check(){
		bool valid = true;
		for (int i = 0; i <= 5; ++i){
			if (!squares[i].check()){
				valid = false;
			}
		}
		return valid;
	}
	Hit intersect(const Ray& ray) {
		Hit bestHit;
		bestHit.t = -1;
		for (int i = 0; i <= 5; ++i){
			Hit tempHit = squares[i].intersect(ray);
			if (tempHit.t > 0 && (bestHit.t == -1 || tempHit.t < bestHit.t)){
				bestHit = tempHit;
			}
		}
		return bestHit;
	}

};

const int U_COUNT = 5;
const int V_COUNT = 6;
class Torus : public Intersectable {
	Triangle triangles[6000000];
	int numTriangles;
	bool initialized;
	float3 modify(float3 in){
		in.rotateX(rotate.x);
		in.rotateY(rotate.y);
		in.rotateZ(rotate.z);
		in = multiply(in, scale);
		in = in + translate;
		return in;
	}
	float3 point(float u, float v){
		u = u * 2 * M_PI;
		v = v * 2 * M_PI;
		float xu = R + r * cos(u);
		float yu = r * sin(u);
		float x = xu * cos(v);
		float y = yu;
		float z = xu * sin(v);
		float3 vertex(x, y, z);
		return modify(vertex);
	}
public:
	float r, R;
	float3 rotate, translate, scale;
	Torus(){
		numTriangles = 0;
		initialized = false;
		scale = float3(1, 1, 1);
	}
	void initialize(){
		numTriangles = 0;

		float du = 1.0 / U_COUNT;
		float dv = 1.0 / V_COUNT;
		for (float u = 0; u <= 1; u += du){
			for (float v = 0; v <= 1; v += dv){
				float3 a, b, c, d;
				a = point(u, v);
				b = point(u, v + dv);
				c = point(u + du, v + dv);
				d = point(u + du, v);
				triangles[numTriangles++] = Triangle(a, c, b);
				triangles[numTriangles++] = Triangle(a, d, c);
			}
		}

		initialized = true;
	}
	Hit intersect(const Ray& ray) {
		if (initialized){
			//befogó gömb
			float a = (ray.v)*(ray.v);
			float b = 2 * (((ray.origin) - translate) * (ray.v));
			float c = (((ray.origin) - translate) * ((ray.origin) - translate)) - negyzet(r+R);
			float d = negyzet(b) - 4 * a*c;
			if (d<0){
				Hit hit;
				hit.t = -1;
				return hit;
			}

			bool originalDebug = debug;
			debug = false;

			Hit bestHit;
			bestHit.t = -1;
			int bestTriangle = -1;
			for (int i = 0; i <= numTriangles - 1; ++i){
				Hit hit = triangles[i].intersect(ray);
				if (bestHit.t < 0 || (hit.t > 0 && hit.t < bestHit.t)){
					bestTriangle = i;
					bestHit = hit;
				}
			}
			if (bestTriangle != -1){
				debug = originalDebug;
				triangles[bestTriangle].intersect(ray);
			}


			bestHit.material = material;
			return bestHit;
		}
		else {
			Hit hit;
			hit.t = -1;
			std::cout << "Uninitalized Torus\n";
			return hit;
		}
	}
};

class Sphere : public Intersectable {
public:
	float3 center;
	float radius;
	Sphere(float3 center, float radius){
		this->center = center;
		this->radius = radius;
	}
	Hit intersect(const Ray& ray) { 
		Hit retVal;
		retVal.material = this->material;

		float a = (ray.v)*(ray.v);
		float b = 2 * (((ray.origin) - center) * (ray.v));
		float c = (((ray.origin) - center) * ((ray.origin) - center)) - negyzet(radius);
		float d = negyzet(b) - 4 * a*c;

		if (d<0){
			retVal.t = -1;
			return retVal;
		}
		else if (d == 0){
			retVal.t = ((-1)*b) / (2 * a);
		}
		else {
			float t1 = ((-1)*b + sqrtf(d)) / (2 * a);
			float t2 = ((-1)*b - sqrtf(d)) / (2 * a);
			/*if (t1<0) t1 = FLT_MAX;
			if (t2<0) t2 = FLT_MAX;

			retVal.t = ((t1<t2) ? (t1) : (t2));*/
			if (t1 < 0 && t2 < 0){
				retVal.t = -1;
			} else if(t1 >= 0 && t2 >=0){
				retVal.t = ((t1<t2) ? (t1) : (t2));
			} else if(t1>=0){
				retVal.t = t1;
			} else {
				retVal.t = t2;
			}
		}

		retVal.position = ray.origin + ray.v * retVal.t;
		if ((ray.origin - center).length()<radius){
			retVal.normal = (center - retVal.position);
		}
		else {
			retVal.normal = (retVal.position - center);
		}

		retVal.normal = retVal.normal.normalize();
		return retVal;
	}
};

struct Light{
	float3 p;
	float3 dir;
	float3 Lout;
	boolean isDir;
	Light(boolean isDir, float3 vec, float3 Lout){
		this->isDir = isDir;
		this->Lout = Lout;
		if (isDir){
			dir = vec;
			p = float3(0, 0, 0);
		}
		else {
			p = vec;
			dir = float3(0, 0, 0);
		}
	}
};

class Camera : public CameraSuper
{
	float3 eye;
	float3 lookAt;
	float3 up;
	float3 right;
	float XM;
	float YM;
	float d;
public:
	Camera(float FOV, float _XM, float _YM, float d){
		this->d = d;
		FOV *= 0.0174532925;
		eye = float3(0, 0, 0);
		lookAt = float3(0, 0, d);
		XM = _XM;
		YM = _YM;
		right = float3(tan(FOV / 2)*d, 0, 0);
		up = float3(0, tan(FOV / 2)*d*(YM / XM), 0);
	}

	Ray getRay(float X, float Y){
		float3 rightTmp = right*(2 * (X + 0.5) / XM - 1);
		float3 upTmp = up*(-1)*(2 * (Y + 0.5) / YM - 1);
		float3 v = lookAt + rightTmp + upTmp;
		float3 p = v + eye;
		v.normalize();
		return Ray(p, v);
	}

	float3 getScreenPoint(float3 worldPoint){
		float3 screenMiddle = eye + lookAt;

		//projecting
		float3 p0 = screenMiddle;
		float3 n = lookAt.normalize();
		float3 l0 = worldPoint;
		float3 l = (eye - worldPoint).normalize();

		if ((l * n) == 0){
			return float3();
		}
		float d = ((p0 - l0) * n) / (l * n);
		float3 projectedPoint = l0 + l * d;

		//base components
		float3 v = projectedPoint - screenMiddle;
		float x = (right.normalize() * v) / right.length();
		float y = (up.normalize() * v) / up.length();

		return float3(x, y, 0);
	}

	std::string toString(){
		std::ostringstream oss;
		oss << "Camera:\n   eye: " << eye.toString() << "\n   lookAt: " << lookAt.toString() << "\n   up: " << up.toString() << "\n   right: " << right.toString() << "\n   XM: " << XM << "\n   YM: " << YM << "\n";
		std::string tmp = oss.str();
		return tmp;
	}
	void yaw(float degree){
		lookAt.rotateY(degree);
		right.rotateY(degree);
		up.rotateY(degree);
	}
	void pitch(float degree){
		lookAt.rotateX(degree);
		right.rotateX(degree);
		up.rotateX(degree);
	}
	void roll(float degree){
		lookAt.rotateZ(degree);
		right.rotateZ(degree);
		up.rotateZ(degree);
	}
	void setPosition(float3 newPos){
		eye = newPos;
	}
};

class Scene{
	Intersectable* objects[300];
	Light* lights[300];
	int objectsCount;
	int lightsCount;
	float3 ambientLight;

	float XM;
	float YM;

	
public:
	float blackHolePower;
	Sphere* blackHoleSphere;
	float3 image[1900 * 1600];
	Camera camera;
	Scene(float _XM, float _YM) :camera(80, _XM, _YM, 0.01){
		objectsCount = 0;
		lightsCount = 0;
		ambientLight = float3(0.1f, 0.1f, 0.1f);

		XM = _XM;
		YM = _YM;
		debugCameraReach = &camera;
	}
	Hit intersectAllObject(Ray ray){
		Hit bestHit;
		for (int i = 0; i <= objectsCount - 1; ++i){
			Hit hit = objects[i]->intersect(ray); //  hit.t < 0 if no intersection
			if (hit.t > 0 && (bestHit.t < 0 || hit.t < bestHit.t))
				bestHit = hit;
		}
		return bestHit;
	}

	Hit firstIntersect(Ray ray) {
		//return intersectAllObject(ray);
		ray.v = ray.v.normalize();
		Ray newRay = ray;
		for (int A = 0; A <= 49; A += 1){
			Hit hit = intersectAllObject(newRay);
			if (hit.t > 0){
				Hit blackHoleHit = (*blackHoleSphere).intersect(newRay);
				if (blackHoleHit.t >= 0 && blackHoleHit.t < 0.011){
					Hit noHit;
					noHit.t = -1;
					return noHit;
				}
				if ((hit.position - newRay.origin).length() < 0.016){
					Hit tempHit = hit;
					tempHit.t = (hit.position - ray.origin).length();
					return tempHit;
				}
			}
			float r = (newRay.origin - blackHoleSphere->center).length();
			float3 blackHoleDir = ((blackHoleSphere->center - newRay.origin).normalize());
			float multiplier = (1 / (pow(r / 0.02, 2))) * (blackHolePower / (375 * pow(10, 23)));
			float3 d = newRay.v + blackHoleDir * multiplier;
			newRay.origin = newRay.origin + newRay.v *	0.01;
			newRay.v = d.normalize();
		}
		Hit noHit;
		noHit.t = -1;
		return noHit;
	}

	void render(){	
		int percent = 0;
		int oldPercent = 0;
		for (int Y = 0; Y<YM; ++Y){
			oldPercent = percent;
			percent = (int)(((float)Y) * 100 / YM);
			if (percent != oldPercent)
				std::cout << percent << "%\n";
			for (int X = 0; X<XM; ++X){
				Ray r = camera.getRay(X, Y);
				debug = false;
				if (X == 300 && Y == 300){
					debug = true;
				}
				//if ((Y*((int)YM) + X) % 10000 == 0){
				//	debug = true;
				//}
				float3 color = trace(r, 0);
				int idx = ((floorl(YM) - Y)*floorl(XM) + X);
				float luminance = color.x * 0.21 + color.y * 0.72 + color.z * 0.07;
				float newLuminance = luminance / (luminance + 1);
				color = color * (newLuminance / luminance);
				image[idx] = color;
			}
		}

	}

	//no directional light support
	float3 trace(Ray ray, int depth) {
		if (depth > 5)
			return float3(0.0f, 0.0f, 0.0f);

		std::string spaces("");
		for (int i = 0; i<depth + 1; ++i) spaces += " +";

		if (debug){
			int a = 5;
		}

		Hit hit = firstIntersect(ray);


		//nothing
		if (hit.t < 0)
			return float3(0.0f, 0.0f, 0.0f); 
		if (debug)
			std::cout << spaces << "hit on: " << hit.material->name << " at: " << hit.position.toString() << "\n";

		if (debug){
			float3 screenPoint1 = camera.getScreenPoint(hit.position);
			float3 screenPoint2 = camera.getScreenPoint(hit.position + hit.normal * 0.01);
			displayPointColors[numDisplayPoints] = float3(1, 0, 0);
			displayPoints[numDisplayPoints++] = screenPoint1;
			displayPointColors[numDisplayPoints] = float3(1, 0, 0);
			displayPoints[numDisplayPoints++] = screenPoint2;
		}

		float3 ambientColor(0, 0, 0);
		float3 roughColor(0, 0, 0);
		float3 reflectionColor(0, 0, 0);
		float3 refractionColor(0, 0, 0);

		float3 viewDir = (ray.origin - hit.position).normalize();

		//rough
		if (hit.material->isRough){
			for (int i = 0; i <= lightsCount - 1; ++i){
				Light* light = lights[i];
				float3 shadowRayDir = (light->p - hit.position).normalize();
				Ray shadowRay(hit.position + shadowRayDir*epsilon, shadowRayDir);
				Hit shadowHit = intersectAllObject(shadowRay);
				if (shadowHit.t < 0 || shadowHit.t >(hit.position - light->p).length()){
					roughColor += hit.material->shade(hit.normal, viewDir, shadowRayDir, light->Lout);
				}
			}
		}
		if (debug)
			std::cout << spaces << "rough color: " << roughColor.toString() << "\n";

		//reflection
		if (hit.material->isReflecting){
			float3 reflectionDir = hit.material->reflect(ray.v, hit.normal).normalize();
			Ray reflectedRay(hit.position + reflectionDir*epsilon, reflectionDir);
			if (debug)
				std::cout << spaces << "tracing reflection\n";
			float3 traced = trace(reflectedRay, depth + 1);
			if (debug)
				std::cout << spaces << "reflection color: " << traced.toString() << "\n";
			reflectionColor += multiply(traced, hit.material->Fresnel(ray.v, hit.normal));
			if (debug)
				std::cout << spaces << "reflection color * fresnel: " << reflectionColor.toString() << "\n";
		}

		//refraction
		if (hit.material->isRefracting) {
			float3 refractionDir = hit.material->refract(ray.v, hit.normal).normalize();
			Ray refractedRay(hit.position + refractionDir*epsilon, refractionDir);

			refractionColor += multiply(trace(refractedRay, depth + 1), (float3(1, 1, 1) - hit.material->Fresnel(ray.v, hit.normal)));
			if (debug)
				std::cout << spaces << "refraction color: " << refractionColor.toString() << "\n";
		}

		//ambient
		ambientColor += multiply(hit.material->ka, ambientLight);
		if (debug)
			std::cout << spaces << "ambient color: " << ambientColor.toString() << "\n";


		float3 outRadiance = ambientColor + roughColor + reflectionColor + refractionColor;
		return outRadiance;
	}

	void addLight(Light* l){
		lights[lightsCount++] = l;
	}

	void addObject(Intersectable* o){
		objects[objectsCount++] = o;
	}

};

	const int screenWidth = 600;
	const int screenHeight = 600;
	Scene scene(screenWidth, screenHeight);

	Material gold;
	Material silver;
	Material glass;
	Material box[10];
	Material torusMaterial;
	float3 f0(float kioltas, float n){
		float tmp = (negyzet(n - 1) + negyzet(kioltas)) / (negyzet(n + 1) + negyzet(kioltas));
		return float3(tmp, tmp, tmp);
	}
	void initMaterials(){
		gold.F0 = f0(2.1, 0.5);
		gold.n = 0.5;
		gold.shininess = 0.4;
		gold.isReflecting = true;
		gold.isRefracting = false;
		gold.isRough = true;
		gold.kd = float3(0.75164, 0.60648, 0.22648);
		gold.ks = float3(0.628281, 0.555802, 0.366065);
		gold.ka = float3(0.24725, 0.1995, 0.0745);
		gold.name = "Gold";

		silver.F0 = f0(3, 0.13);
		silver.n = 0.13;
		//silver.shininess = 0.4;
		silver.shininess = 40;
		silver.isReflecting = true;
		silver.isRefracting = false;
		silver.isRough = true;
		silver.kd = float3(0.50754, 0.50754, 0.50754);
		silver.ks = float3(0.508273, 0.508273, 0.508273);
		silver.ka = float3(0.19225, 0.19225, 0.19225);
		silver.name = "Silver";

		torusMaterial.F0 = f0(20, 0.13);
		torusMaterial.n = 0.13;
		//torusMaterial.shininess = 0.4;
		torusMaterial.shininess = 1;
		torusMaterial.isReflecting = true;
		torusMaterial.isRefracting = false;
		torusMaterial.isRough = true;
		torusMaterial.kd = float3(0.50754, 0.50754, 0.50754);
		torusMaterial.ks = float3(0.508273, 0.508273, 0.508273);
		torusMaterial.ka = float3(0.19225, 0.19225, 0.19225);
		torusMaterial.name = "TorusMaterial";

		glass.F0 = f0(0, 1.5);
		glass.n = 1.5;
		glass.shininess = 0.4;
		glass.isReflecting = true;
		glass.isRefracting = true;
		glass.isRough = false;
		glass.kd = float3(0, 0, 0);
		glass.ks = float3(0, 0, 0);
		glass.ka = float3(0, 0, 0);
		glass.name = "Glass";

		for (int i = 0; i <= 11; ++i){
			box[i].F0 = f0(3, 0.13);
			box[i].n = 0.13;
			box[i].shininess = 0.01;
			box[i].isReflecting = false;
			box[i].isRefracting = false;
			box[i].isRough = true;
			box[i].kd = float3(0.8, 0, 0);
			box[i].ks = float3(0.8, 0, 0);
			box[i].ka = float3(0.8, 0, 0);
			box[i].name = "Box";
			box[i].name.append(std::to_string((i/2)+1));
		}
		box[0].kd = float3(0.8, 0, 0);
		box[0].ks = float3(0.8, 0, 0);
		box[0].ka = float3(0.8, 0, 0);

		box[1].kd = float3(0, 0.3, 0);
		box[1].ks = float3(0, 0.3, 0);
		box[1].ka = float3(0, 0.3, 0);

		box[2].kd = float3(0, 0, 0.8);
		box[2].ks = float3(0, 0, 0.8);
		box[2].ka = float3(0, 0, 0.8);

		box[3].kd = float3(0.4, 0.4, 0);
		box[3].ks = float3(0.4, 0.4, 0);
		box[3].ka = float3(0.4, 0.4, 0);

		box[4].kd = float3(0.4, 0.4, 0.7);
		box[4].ks = float3(0.4, 0.4, 0.7);
		box[4].ka = float3(0.4, 0.4, 0.7);

		box[5].kd = float3(0.3, 0.3, 0);
		box[5].ks = float3(0.3, 0.3, 0);
		box[5].ka = float3(0.3, 0.3, 0);

		box[6].kd = float3(0, 0.3, 0.8);
		box[6].ks = float3(0, 0.3, 0.8);
		box[6].ka = float3(0, 0.3, 0.8);

		box[7].kd = float3(0.2, 0, 0.4);
		box[7].ks = float3(0.2, 0, 0.4);
		box[7].ka = float3(0.2, 0, 0.4);

		box[8].kd = float3(0, 0, 0);
		box[8].ks = float3(0, 0, 0);
		box[8].ka = float3(0, 0, 0);

		box[9].kd = float3(1, 1, 1);
		box[9].ks = float3(1, 1, 1);
		box[9].ka = float3(1, 1, 1);

		box[10].kd = float3(0.7, 0, 0.1);
		box[10].ks = float3(0.7, 0, 0.1);
		box[10].ka = float3(0.7, 0, 0.1);

		box[11].kd = float3(0.2, 0.1, 0);
		box[11].ks = float3(0.2, 0.1, 0);
		box[11].ka = float3(0.2, 0.1, 0);

	}

//	Sphere sphere1(float3(-3.5, 0, 3), 1);
	Sphere sphereBH(float3(-0.02, 0, 0.025), 0.011);
	//	Sphere sphere2(float3(1.5, 0, 3), 2.5);
	Torus torus;
	//TexturedSquare square;
	OpenBox openBox;
	void initObjects(){
		//sphere1.material = &gold;
		//sphere2.material = &silver;

		//square.a = float3(4, 4, 6);
		//square.b = float3(4, -4, 6);
		//square.c = float3(-4, -4, 6);
		//square.d = float3(-4, 4, 6);
		//square.mat1 = &box1;
		//square.mat2 = &box2;
		openBox.p[0] = float3(-0.05, -0.05, -0.05);
		openBox.p[1] = float3(0.05, -0.05, -0.05);
		openBox.p[2] = float3(0.05, 0.05, -0.05);
		openBox.p[3] = float3(-0.05, 0.05, -0.05);

		openBox.p[4] = float3(-0.05, -0.05, 0.05);
		openBox.p[5] = float3(0.05, -0.05, 0.05);
		openBox.p[6] = float3(0.05, 0.05, 0.05);
		openBox.p[7] = float3(-0.05, 0.05, 0.05);

		for (int i = 0; i <= 11; ++i){
			openBox.materials[i] = &box[i];
		}

		openBox.initalize();
		openBox.check();

		torus.material = &gold;
		torus.r = 0.004;
		torus.R = 0.0125;
		torus.rotate = float3(-90, 30, 0);
		torus.translate = float3(0.0125, 0, 0.025);
		torus.initialize();
	}

	Light light1(false, float3(0.03, 0.008, -0.016), float3(3, 3, 3));
	//Light light2(false, float3(0, 0, -4), float3(3, 3, 3));
	void onInitialization() {
		glViewport(0, 0, screenWidth, screenHeight);

		//scene.blackHolePosition = float3(-0.02, 0, 0.025);
		scene.blackHolePower = 6 * pow(10,24);
		scene.blackHoleSphere = &sphereBH;
		initMaterials();
		initObjects();

		//scene.addObject(&sphere1);
		//scene.addObject(&sphere2);
		scene.addObject(&torus);
		scene.addObject(&openBox);
		//scene.addObject(&square);
		scene.addLight(&light1);
		//scene.addLight(&light2);
		//std::cout << scene.camera.toString();
		scene.camera.setPosition(float3(0, 0, -0.03));


		long time = glutGet(GLUT_ELAPSED_TIME);
		scene.render();
		std::cout << "render time: " << glutGet(GLUT_ELAPSED_TIME) - time << "ms\n";
	}
	void onDisplay() {
		glClearColor(0.1f, 0.2f, 0.3f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); 
		glDrawPixels(screenWidth, screenHeight, GL_RGB, GL_FLOAT, scene.image);

		glPointSize(3.0);
		glBegin(GL_LINES);
		for (int i = 0; i <= numDisplayPoints - 1; ++i){
			float3 tmp1 = displayPoints[i];
			float3 tmp2 = displayPointColors[i];
			glColor3f(tmp2.x, tmp2.y, tmp2.z);
			glVertex3f(tmp1.x, tmp1.y, tmp1.z);
		}
		glEnd();

		glutSwapBuffers();     	
	}

	void onMouse(int button, int state, int x, int y) {
		if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
			float3 tmp(x, y);
			std::cout << tmp.toString() << "\n";
			Ray r = scene.camera.getRay(x, y);
			debug = true;
			float3 color = scene.trace(r, 0);
			debug = false;

		}
		glutPostRedisplay(); 						
	}

	void onKeyboard(unsigned char key, int x, int y) {}
	void onIdle() {}
	void onKeyboardUp(unsigned char key, int x, int y) {}
	void onMouseMotion(int x, int y){}
// ...Idaig modosithatod
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

// A C++ program belepesi pontja, a main fuggvenyt mar nem szabad bantani
int main(int argc, char **argv) {
	glutInit(&argc, argv); 				// GLUT inicializalasa
	glutInitWindowSize(600, 600);			// Alkalmazas ablak kezdeti merete 600x600 pixel 
	glutInitWindowPosition(100, 100);			// Az elozo alkalmazas ablakhoz kepest hol tunik fel
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE | GLUT_DEPTH);	// 8 bites R,G,B,A + dupla buffer + melyseg buffer

	glutCreateWindow("Grafika hazi feladat");		// Alkalmazas ablak megszuletik es megjelenik a kepernyon

	glMatrixMode(GL_MODELVIEW);				// A MODELVIEW transzformaciot egysegmatrixra inicializaljuk
	glLoadIdentity();
	glMatrixMode(GL_PROJECTION);			// A PROJECTION transzformaciot egysegmatrixra inicializaljuk
	glLoadIdentity();

	onInitialization();					// Az altalad irt inicializalast lefuttatjuk

	glutDisplayFunc(onDisplay);				// Esemenykezelok regisztralasa
	glutMouseFunc(onMouse);
	glutIdleFunc(onIdle);
	glutKeyboardFunc(onKeyboard);
	glutKeyboardUpFunc(onKeyboardUp);
	glutMotionFunc(onMouseMotion);

	glutMainLoop();					// Esemenykezelo hurok

	return 0;
}
